import React from "react";
import { Link } from "react-router-dom";
import { useMeQuery, useLogoutMutation } from "./generated/graphql";
import { setAccessToken } from "./accessToken";

export const Header: React.FC = () => {
  const { data, loading } = useMeQuery();
  const [logout, { client }] = useLogoutMutation();

  let body: any = null;

  if (loading) {
    body = null;
  } else if (data && data.me) {
    body = <div>Bem vindo {data.me.email}</div>;
  } else {
    body = <div>Usuario nao logado</div>;
  }

  return (
    <div>
      <header>
        <div>
          <Link to={"/cadastro"}>Cadastro</Link>
        </div>
        <div>
          <Link to={"/"}>Home</Link>
        </div>
        <div>
          <Link to={"/login"}>Login</Link>
        </div>
        <div>
          <Link to={"/bye"}>Bye</Link>
        </div>
        {!loading && data && data.me ? (
          <button
            onClick={async () => {
              await logout();
              setAccessToken("");
              await client!.resetStore();
            }}
          >
            Logout
          </button>
        ) : null}
        {body}
      </header>
    </div>
  );
};
